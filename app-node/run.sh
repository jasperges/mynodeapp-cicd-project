#!/bin/sh

set -e

print_usage() {
    echo "Usage: $0 <command>"
    echo "Where <command> is one of: start, test"
}

error_usage() {
    print_usage
    exit 1
}

_install() {
    npm install
}

_start() {
    npm start
}

_test() {
    _install
    npm test
}

[ -z "$*" ] && error_usage

case "$1" in
    install) _install ;;
    start) _start ;;
    test) _test ;;
    *) error_usage ;;
esac
