package main

import (
	"errors"
	"log"
	"os"
	"testing"
)

func TestIndexExists(t *testing.T) {
	currentWorkingDirectory := getwd()
	if _, err := os.Stat(currentWorkingDirectory + "/index.html"); errors.Is(err, os.ErrNotExist) {
		t.Errorf("index.html doesn't exist")
	}
}

func TestDockerfileExists(t *testing.T) {
	currentWorkingDirectory := getwd()
	if _, err := os.Stat(currentWorkingDirectory + "/../Dockerfile-go"); errors.Is(err, os.ErrNotExist) {
		t.Errorf("Dockerfile doesn't exist")
	}
}
func TestGitignoreExists(t *testing.T) {
	currentWorkingDirectory := getwd()
	if _, err := os.Stat(currentWorkingDirectory + "/../.gitignore"); errors.Is(err, os.ErrNotExist) {
		t.Errorf(".gitignore doesn't exist")
	}
}

func getwd() string {
	currentWorkingDirectory, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	return currentWorkingDirectory
}
