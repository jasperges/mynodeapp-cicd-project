#!/bin/sh

set -e

print_usage() {
    echo "Usage: $0 <command>"
    echo "Where <command> is one of: build, start, test"
}

error_usage() {
    print_usage
    exit 1
}

_build() {
    go build .
}

_start() {
    ./app
}

_test() {
    apk add --no-cache --virtual .build-deps gcc musl-dev
    go install github.com/jstemmer/go-junit-report/v2@latest
    go test -v 2>&1 | go-junit-report -set-exit-code > junit.xml
    apk del .build-deps
}

[ -z "$*" ] && error_usage

case "$1" in
    build) _build ;;
    start) _start ;;
    test) _test ;;
    *) error_usage ;;
esac
