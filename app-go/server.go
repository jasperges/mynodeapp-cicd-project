package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	http.HandleFunc("/profile-picture-andrea", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "images/profile-andrea.jpg")
	})
	http.HandleFunc("/profile-picture-ari", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "images/profile-ari.jpeg")
	})
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "index.html")
	})
	fmt.Println("app listening on port 3000: http://localhost:3000")
	log.Fatal(http.ListenAndServe(":3000", nil))
}
