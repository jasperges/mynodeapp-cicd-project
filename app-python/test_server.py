from pathlib import Path

APP_DIR = Path(__file__).parent


def test_index_exists() -> None:
    """Test if the main index.html exists."""
    assert (APP_DIR / "index.html").is_file()


def test_dockerfile_exists() -> None:
    """Test if the main index.html exists."""
    assert (APP_DIR.parent / "Dockerfile-python").is_file()


def test_gitignore_exists() -> None:
    """Test if the main index.html exists."""
    assert (APP_DIR.parent / ".gitignore").is_file()
