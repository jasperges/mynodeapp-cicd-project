from fastapi import FastAPI
from fastapi.params import File
from starlette.responses import FileResponse, HTMLResponse

app = FastAPI()


@app.get("/profile-picture-andrea", response_class=FileResponse)
async def profile_andrea():
    headers = {"Content-Type": "image/jpg"}
    return FileResponse("images/profile-andrea.jpg", headers=headers)


@app.get("/profile-picture-ari", response_class=FileResponse)
async def profile_ari():
    headers = {"Content-Type": "image/jpg"}
    return FileResponse("images/profile-ari.jpeg", headers=headers)


@app.get("/", response_class=HTMLResponse)
async def root():
    with open("index.html") as index:
        return index.read()
