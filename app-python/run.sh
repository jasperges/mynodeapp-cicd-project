#!/bin/sh

set -e

print_usage() {
    echo "Usage: $0 <command>"
    echo "Where <command> is one of: install, start, test"
}

error_usage() {
    print_usage
    exit 1
}

[ -z "$*" ] && error_usage

export PATH="${HOME:-root}/.local/bin:${PATH}"

_install() {
    apk add --no-cache --virtual .build-deps curl \
        && curl -sSL https://install.python-poetry.org | python3 - \
        && apk del .build-deps \
        && poetry install
}

_start() {
    poetry run server.py
}

_test() {
    _install
    poetry run pytest --junit-xml=./junit.xml
}

case "$1" in
    install) _install ;;
    start) _start ;;
    test) _test ;;
    *) error_usage ;;
esac
